﻿using Automatonymous;

public class Relationship
{
    public State CurrentState { get; set; }

    public string Name { get; set; }
}

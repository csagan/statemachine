﻿using Automatonymous;

using System;

namespace StateMachine
{
    class Program
    {
        static void Main(string[] args)
        {
            var relationship = new Relationship { Name = "John" };
            var machine = new RelationshipStateMachine();

            machine.RaiseEvent(relationship, machine.Hello);

            Console.WriteLine(relationship.CurrentState.Name);

            Console.ReadKey();
        }
    }
}
